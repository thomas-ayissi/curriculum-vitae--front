# Curriculum Vitae (frontend)

## This repository

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.
This repository presents your professionnal life with an awesome design.

## How to install and use it ?

1. Install [Node.js](https://nodejs.org/en/download/)
2. Install [Angular CLI](https://cli.angular.io/)
3. Clone this repo
4. In the project folder (example: ~/git/curriculum-vitae--frontend),
   run `npm install` | `npm i`

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

#### Run the project with development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

#### Deploy on FireBase using GitLab CI/CD

If you need more information you can check the [FireBase Documentation](https://firebase.google.com/docs) and the [Angular Documentation](https://angular.io/guide/deployment#fallback-configuration-examples)

## Contributing

* Create an issue and explain your problem
* Fork it / Create your branch / Commit your changes / Push to your branch / Submit a pull request

## Maintainer

Luc AUCOIN <luc.aucoin1998@gmail.com>
