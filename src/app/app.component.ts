import {Component, OnInit} from '@angular/core';
import {Profile} from './models/profile.model';
import {ProfileService} from './services/profile.service';
import {doOnSubscribe} from './modules/reactive.ext';
import {finalize} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import getUserLocale from 'get-user-locale';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  profile: Profile;
  isDataLoaded: boolean;

  isError = false;

  constructor(
    private profileService: ProfileService,
    private translator: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.getLanguage();
  }

  private getLanguage(): void {
    let lang;

    if (getUserLocale().includes('fr')) {
      lang = 'fr';
    } else {
      lang = 'en';
    }

    this.translator.use(lang);
    this.getProfile(lang);
  }

  private getProfile(lang: string): void {
    this.profileService.getProfile(lang)
      .pipe(
        doOnSubscribe(() => this.showLoader()),
        finalize(() => this.hideLoader())
      )
      .subscribe(profile => {
        this.profile = profile;
      }, error => {
        this.isError = true;
        console.log(error);
      });
  }

  private showLoader(): void {
    this.isDataLoaded = false;
  }

  private hideLoader(): void {
    this.isDataLoaded = true;
  }
}
