import {Component, EventEmitter, Input, Output} from '@angular/core';
import {environment} from "../../../environments/environment";
import {General} from "../../models/general.model";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['../../app.component.css', './header.component.css']
})
export class HeaderComponent {
  @Input() profile: General;
  url = environment.url;
}
