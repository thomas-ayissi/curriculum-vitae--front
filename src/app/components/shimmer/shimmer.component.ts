import { Component } from '@angular/core';

@Component({
  selector: 'app-shimmer',
  templateUrl: './shimmer.component.html',
  styleUrls: ['../../app.component.css', './shimmer.component.css']
})
export class ShimmerComponent { }
