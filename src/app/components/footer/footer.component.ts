import {Component, Input} from '@angular/core';
import {Link} from "../../models/link.model";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {
  @Input() links: Link[]
  url = environment.url
}
