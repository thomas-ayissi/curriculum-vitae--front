import {Component, Input} from '@angular/core';
import {Education} from "../../../models/education.model";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['../../../app.component.css', '../main.component.css', './education.component.css']
})
export class EducationComponent {
  @Input() education: Education[];
  url = environment.url
}
