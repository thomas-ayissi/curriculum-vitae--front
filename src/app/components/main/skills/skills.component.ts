import {Component, Input} from '@angular/core';
import {Skills} from "../../../models/skill.model";

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['../../../app.component.css', '../main.component.css', './skills.component.css']
})
export class SkillsComponent {
  @Input() skills: Skills;
}
