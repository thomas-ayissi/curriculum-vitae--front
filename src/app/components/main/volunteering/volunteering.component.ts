import {Component, Input} from '@angular/core';
import {VolunteerExperience} from "../../../models/volunteerExperience.model";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-volunteering',
  templateUrl: './volunteering.component.html',
  styleUrls: ['../../../app.component.css', '../main.component.css', './volunteering.component.css']
})
export class VolunteeringComponent {
  @Input() experiences: VolunteerExperience[];
  url = environment.url
}
