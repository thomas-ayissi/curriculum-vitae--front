import {Component, Input} from '@angular/core';
import {ProfessionalExperience} from "../../../models/professionalExperience.model";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-experiences',
  templateUrl: './experiences.component.html',
  styleUrls: ['../../../app.component.css', '../main.component.css', './experiences.component.css']
})
export class ExperiencesComponent {
  @Input() experiences: ProfessionalExperience[];
  url = environment.url
}
