import {Component, Input} from '@angular/core';
import {Certification} from "../../../models/certification.model";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-certifications',
  templateUrl: './certifications.component.html',
  styleUrls: ['../../../app.component.css', '../main.component.css', './certifications.component.css']
})
export class CertificationsComponent {
  @Input() certifications: Certification[];
  url = environment.url
}
