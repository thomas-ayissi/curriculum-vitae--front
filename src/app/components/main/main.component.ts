import {Component, Input} from '@angular/core';
import {Data} from "../../models/data.model";
import {Mailer} from "../../models/mailer.model";
import emailjs from 'emailjs-com';
import {Mail} from "../../models/mail.model";
import {MatSnackBar} from "@angular/material/snack-bar";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent {
  @Input() data: Data
  @Input() mailer: Mailer

  public loading = false

  constructor(
    private snackBar: MatSnackBar,
    private translate: TranslateService
  ) { }

  sendMail(name: string, mail: string, message: string): void {
    this.loading = true
    emailjs.send(
      this.mailer.service,
      this.mailer.template,
      new Mail(name, mail, message),
      this.mailer.user
    )
      .then( _ => {
        this.loading = false
        this.translate.get('mailer.success')
          .subscribe( message =>
            this.snackBar.open(message, "OK !", {
              duration: 2000
            })
          )
      }, _ => {
        this.loading = false
        this.translate.get('mailer.error')
          .subscribe( message =>
            this.snackBar.open(message, "OK !", {
              duration: 2000
            })
          )
      });
  }
}
