import {Component, Input} from '@angular/core';
import {Project} from "../../../models/project.model";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['../../../app.component.css', '../main.component.css', './projects.component.css']
})
export class ProjectsComponent {
  @Input() projects: Project[];
}
