import {defer, Observable} from "rxjs";

export const doOnSubscribe = (onSubscribe: () => void) => <T>(source: Observable<T>) =>
  defer(() => {
    onSubscribe();
    return source;
  });
