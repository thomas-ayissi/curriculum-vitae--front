import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NgxSkeletonLoaderModule} from 'ngx-skeleton-loader';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from "./modules/material.module";
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {ShimmerComponent} from './components/shimmer/shimmer.component';
import {HeaderComponent} from './components/header/header.component';
import {MainComponent} from './components/main/main.component';
import {FooterComponent} from './components/footer/footer.component';
import {ExperiencesComponent} from './components/main/experiences/experiences.component';
import {CertificationsComponent} from './components/main/certifications/certifications.component';
import {EducationComponent} from './components/main/education/education.component';
import {SkillsComponent} from './components/main/skills/skills.component';
import {VolunteeringComponent} from './components/main/volunteering/volunteering.component';
import {ProjectsComponent} from './components/main/projects/projects.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/');
}

@NgModule({
  declarations: [
    AppComponent,
    ShimmerComponent,
    HeaderComponent,
    MainComponent,
    FooterComponent,
    ExperiencesComponent,
    CertificationsComponent,
    EducationComponent,
    SkillsComponent,
    VolunteeringComponent,
    ProjectsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxSkeletonLoaderModule,
    BrowserAnimationsModule,
    MaterialModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
