export class Mail {
  fromName: string;
  replyTo: string;
  message: string;

  constructor(fromName: string, replyTo: string, message: string) {
    this.fromName = fromName;
    this.replyTo = replyTo;
    this.message = message;
  }
}
