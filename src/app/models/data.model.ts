import {ProfessionalExperience} from "./professionalExperience.model";
import {VolunteerExperience} from "./volunteerExperience.model";
import {Education} from "./education.model";
import {Skills} from "./skill.model";
import {Certification} from "./certification.model";
import {Project} from "./project.model";

export class Data {
  professionalExperiences: ProfessionalExperience[];
  education: Education[];
  skills: Skills;
  certifications: Certification[];
  volunteerExperiences: VolunteerExperience[];
  projects: Project[];
}
