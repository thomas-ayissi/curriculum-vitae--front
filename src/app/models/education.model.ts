import {Company} from './company.model';

export class Education {
  dates: string;
  degree: string;
  description: string[];
  company: Company;
}
