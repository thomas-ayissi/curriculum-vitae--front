export class General {
  photo: string;
  firstName: string;
  lastName: string;
  currentPosition: string;
  email: string;
  phone: string;
  languages: string[];
  address: string;
  city: string;
  country: string;
  locationUrl: string;
}
