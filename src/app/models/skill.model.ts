export class Skills {
  management: Skill[];
  technical: Skill[];
}

export class Skill {
  name: string;
  usage: string;
  level: string;
}
