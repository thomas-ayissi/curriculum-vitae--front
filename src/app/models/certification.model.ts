import {Company} from './company.model';

export class Certification {
  dates: string;
  title: string;
  company: Company;
}
