export class Company {
  logo: string;
  name: string;
  location: string;
}
