import {Link} from './link.model';
import {Data} from './data.model';
import {General} from "./general.model";
import {Mailer} from "./mailer.model";

export class Profile {
  general: General;
  links: Link[];
  data: Data;
  mailer: Mailer;
}
