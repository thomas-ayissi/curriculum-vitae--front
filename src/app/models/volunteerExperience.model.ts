import {Company} from './company.model';

export class VolunteerExperience {
  dates: string;
  title: string;
  descriptions: string[];
  company: Company;
}
