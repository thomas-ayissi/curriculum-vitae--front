export class Project {
  name: string;
  availabilityUrl: string;
  gitLabUrl: string;
  gitHubUrl: string;
}
