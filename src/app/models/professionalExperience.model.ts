import {Company} from './company.model';

export class ProfessionalExperience {
  dates: string;
  title: string;
  descriptions: string[];
  company: Company;
}
