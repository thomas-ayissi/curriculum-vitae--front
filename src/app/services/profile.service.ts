import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import {Profile} from '../models/profile.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private client: HttpClient) { }

  public getProfile(lang: string): Observable<Profile> {
    return this.client.get<Profile>(`${environment.url}/${lang.toLowerCase()}.json`);
  }
}
