<a name="2.1.2"></a>
## 2.1.2 (2021-01-06)


#### Bug Fixes

*   format changelog ([77619b79](https://gitlab.com/Luc-AUCOIN/curriculum-vitae--frontend/commit/77619b79618987a051bf2440ab3ece752d2f6136))



## 2.0.1

Feat:
 - Add 404 page
 - Manage error on service

## 2.0.0

Breaking change:
 - Use RXJS in order to get data
 - Use just on call on data source

Feat:
 - Add shimmer layout
 - Add contact form
 - Update CI/CD

## 1.2.0

Feat:
 - Replace GitHub Action (Deploy on GitHub Page) with GitLab CI/CD (Deploy on Firebase)  

## 1.1.0

Feature:
 - Implement the CI/CD to automatize the deployment when a push was made on the branch "develop".
